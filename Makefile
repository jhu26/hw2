# Test Makefile Parallel Performance
# Author: Julia Hu

JCC = javac

all: CoinFlip.class BruteForceDES.class

CoinFlip.class: CoinFlip.java
	$(JCC) CoinFlip.java

BruteForceDES.class: BruteForceDES.java
	$(JCC) BruteForceDES.java 

clean: 
	rm -r *.class
