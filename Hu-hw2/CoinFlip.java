/**
 * Julia Hu, Coin Flip, Assignment 2, Parallel Programming
 */

import java.lang.Thread;
import java.util.Random;
//import java.util.concurrent.atomic.AtomicInteger;

public class CoinFlip implements Runnable {
	
	long numFlips;
	int heads;
	int tails;
	Random ranObject;
	
	//constructor
	public CoinFlip(int id, long flips) {
		this.numFlips = flips;
    	this.heads = 0;
    	this.tails = 0;
		this.ranObject = new Random();
	}

	public int getHeads() {
		return this.heads;
	}
	public int getTails() {
		return this.tails;
	}
	
	public void run() {
		
		int randomNum;
		
		for (long i = 0; i < numFlips; i++) {
			randomNum = ranObject.nextInt(2);
			
			if (randomNum == 0) {
				this.heads++;
			}
			else if(randomNum == 1) {
				this.tails++;
			}
			else if ((randomNum!=0) && (randomNum!=1)) {
				System.out.println("Random Number generator error.");
			}
		}
		
	}
	
	public static void main(String args[]) {

		long numThreads = Long.parseLong(args[0]);
		long numFlips = Long.parseLong(args[1]);
		int numHeads = 0;
		int numTails = 0;
		
		long initialTime = System.currentTimeMillis();
		
		Thread[] threads = new Thread[(int)numThreads];
		CoinFlip[] arrayFlips = new CoinFlip[(int)numThreads];

		//long startupCost = 0; 
		long begStartupCost = System.currentTimeMillis();

		for (int i = 0; i < numThreads; i++) {
			arrayFlips[i] = new CoinFlip(i, numFlips/numThreads);
			threads[i] = new Thread (arrayFlips[i]);
			threads[i].start();
		}
		long startupCost = System.currentTimeMillis() - begStartupCost;

		//long startupFinalTime = System.currentTimeMillis() - startupStartTime;
		
		for (int i = 0; i < numThreads; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
		         System.out.println("Thread interrupted.  Exception: " + e.toString() +
                         " Message: " + e.getMessage()) ;
		         return;
			}
		}

		for (int i = 0; i < numThreads; i++) {
			numHeads = numHeads + arrayFlips[i].getHeads();
			numTails = numTails + arrayFlips[i].getTails();
		}
		
		System.out.println(numHeads + " heads in " + numFlips + " coin tosses.");
		System.out.println(numTails + " tails in " + numFlips + " coin tosses.");
		System.out.println("Startup costs: " + startupCost + " ms");
		System.out.println("Elapsed time: " + (System.currentTimeMillis() - initialTime) + " ms");
	}
}
