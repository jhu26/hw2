/** Julia Hu, parallelSealedDES.java, Assignment 2 **/


import javax.crypto.*;
import java.security.*;
import javax.crypto.spec.*;

import java.lang.Thread;
import java.util.Random;

import java.io.PrintStream;

public class parallelSealedDES implements Runnable {
	int thread_id;
	long maxKeys;
	SealedObject sldObj;
	long runstart;

	public parallelSealedDES(int id, long numKeys, SealedObject sldObj, long runstart) {
		this.thread_id = id;
		this.maxKeys = numKeys;
		this.sldObj = sldObj;
		this.runstart = runstart;
	}

	public void run() {
		SealedDES deccipher = new SealedDES ();

		for (long i = thread_id*maxKeys; i < (thread_id+1)*(this.maxKeys); i++) {
			deccipher.setKey(i);
			String decryptstr = deccipher.decrypt(this.sldObj);

			// create object to printf to the console
			PrintStream p = new PrintStream(System.out);

			if (( decryptstr != null ) && ( decryptstr.indexOf ( "Hopkins" ) != -1 ))
			{
				//  Remote printlns if running for time.
				//p.printf("Found decrypt key %016x producing message: %s\n", i , decryptstr);
				System.out.println ( "Thread " + thread_id + " Found decrypt key " + i + " producing message: " + decryptstr );
			}
			
			// Update progress every once in awhile.
			//  Remote printlns if running for time.
			if ( i % 100000 == 0 )
			{ 
				long elapsed = System.currentTimeMillis() - this.runstart;
			//	System.out.println ("Thread " + this.thread_id + " Searched key number " + i + " at " + elapsed + " milliseconds.");
			}
		}
	}

}

